#!/bin/bash

# Définition du répertoire de travail
WKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# Arrrêt  des conteneurs
docker-compose -f "$WKDIR/docker-compose.yml" stop $*
