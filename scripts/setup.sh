#!/bin/bash

# Définition du répertoire de travail
WKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

echo $WKDIR

source $WKDIR/.env

#création des volumes persistants
mkdir -p $WKDIR/$DATA_DIR/{nextcloud/apps,nextcloud/config,nextcloud/data,nextcloud/postgres,redis,traefik,jellyfin/config,jellyfin/cache}


# Redis
sudo sysctl vm.overcommit_memory=1
sudo sysctl net.core.somaxconn=4096
echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
echo never | sudo tee /sys/kernel/mm/transparent_hugepage/defrag
sudo sysctl -p

# Gestion des partages NFS
sudo apt install nfs-common

