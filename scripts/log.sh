#!/bin/bash

# Définition du répertoire de travail
WKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

docker-compose -f "$WKDIR/docker-compose.yml" logs -f $*
